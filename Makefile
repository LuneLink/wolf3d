# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/20 14:59:02 by spetrenk          #+#    #+#              #
#    Updated: 2019/04/20 14:59:04 by spetrenk         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d

CC = gcc

CFLAGS = -Wall -Wextra -Werror

INCLUDES =  includes/

SRCS =	main.c \
		srcs/player/player.c \
		srcs/error_handler/sdl_init_error.c \
		srcs/scene/init_scene.c \
		srcs/scene/init_texture.c \
		srcs/scene/read_map.c \
		srcs/scene/ft_strsplit.c \
		srcs/scene/ft_strchr.c \
		srcs/scene/ft_memmove.c \
		srcs/scene/ft_memcpy.c \
		srcs/scene/ft_strdup.c \
		srcs/scene/ft_strlen.c \
		srcs/scene/ft_strnew.c \
		srcs/scene/gnl.c \
		srcs/scene/ft_strjoin.c \
		srcs/scene/ft_strcat.c \
		srcs/error_handler/no_file.c \
		srcs/error_handler/map_error.c \
		srcs/scene/ft_atoi.c \
		srcs/scene/ft_isspace.c \
		srcs/scene/ft_isdigit.c \
		srcs/scene/clear_split.c \
		srcs/scene/ft_memalloc.c \
		srcs/scene/put_pixel.c \
		srcs/scene/game_loop.c


OBJ = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L/Users/spetrenk/.brew/Cellar/sdl2/2.0.8/lib -lSDL2 -lm -framework OpenGL -framework AppKit

%.o: %.c
	$(CC) -I $(INCLUDES) -I /Users/spetrenk/.brew/Cellar/sdl2/2.0.8/include/SDL2 $(CFLAGS) -c  -o $@ $<

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re:  fclean all

norm:
	norminette $(INCLUDES)
	norminette $(SRCS)

rmsh:
	find . -name ".DS*" -o -name "._.*" -o -name "#*" -o -name "*~" -o -name "*.out" > rmsh
	xargs rm < rmsh
	rm rmsh
