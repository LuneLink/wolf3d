/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_handler.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 20:45:27 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 20:45:38 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_ERROR_HANDLER_H
# define WOLF3D_ERROR_HANDLER_H

# include <unistd.h>
# include <stdlib.h>

int		sdl_init_error(void);
void	wrong_symb_in_line(void);
void	wrong_map_size_param(void);
void	wrong_count_in_line(void);
void	no_position_for_player(void);
void	map_should_contain_border(void);
void	no_file(void);

#endif
