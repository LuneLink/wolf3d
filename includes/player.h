/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 20:45:44 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 20:45:47 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER_H
# define PLAYER_H
# define R_SPEED 0.1

# include <stdlib.h>
# include <math.h>

typedef struct	s_player
{
	float		pos_x;
	float		pos_y;
	float		dir_x;
	float		dir_y;
	float		plane_x;
	float		plane_y;
	float		fov;
}				t_player;

void			rotate(t_player *p, int is_right);
void			move(t_player *p, char **map, int is_toward);

#endif
