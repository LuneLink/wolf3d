/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 20:45:53 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 20:45:55 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_SCENE_H
# define WOLF3D_SCENE_H
# define BUFF_SIZE 500
# define ABS(a) (((a) < (0)) ? (-a) : (a))

# include "error_handler.h"
# include "player.h"
# include <SDL2/SDL.h>
# include <unistd.h>
# include <errno.h>
# include <fcntl.h>

typedef struct		s_render
{
	float			camera_x;
	float			ray_d_x;
	float			ray_d_y;
	int				map_x;
	int				map_y;
	float			side_dist_x;
	float			side_dist_y;
	float			delta_dist_x;
	float			delta_dist_y;
	float			prep_wall_dist;
	int				step_x;
	int				step_y;
	int				hit;
	int				side;
	float			wall_x;
	int				draw_s;
	int				draw_e;
	int				tex_numb;
	int				color;
	int				tex_x;
	int				tex_y;
	int				d;
	int				y_ebani;
}					t_render;

typedef struct		s_scene {
	SDL_Surface		*surface;
	SDL_Renderer	*renderer;
	SDL_Window		*window;
	t_player		player;
	char			**map;
	int				map_w;
	int				map_h;
	int				w;
	int				h;
	int				text_w;
	int				text_h;
	unsigned		textures[8][64 * 64];
	t_render		render;
}					t_scene;

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

int					init_scene(t_scene *scene, int w, int h);
void				init_texture(t_scene *scene);
void				read_map(t_scene *scene, char *file);
void				putpixels(SDL_Surface *screen,
							int x, int y, unsigned int color);
void				game_loop(t_scene *scene);

void				*ft_memalloc(size_t size);
int					clear_split(char ***splited);
int					ft_atoi(const char *str);
int					ft_isdigit(int ch);
int					ft_isspace(int ch);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strcat(char *destination, const char *source);
char				*ft_strnew(size_t size);
char				*ft_strdup(const char *s);
size_t				ft_strlen(const char *s);
void				*ft_memmove(void *destination,
						const void *source, size_t count);
void				*ft_memcpy(void *destination,
						const void *source, size_t count);
char				*ft_strchr(const char *s, int c);
char				**ft_strsplit(char const *s, char c);
int					get_next_line(const int fd, char **line);

#endif
