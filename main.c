/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 21:25:11 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 21:25:14 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <SDL2/SDL.h>
#include "scene.h"
#include "error_handler.h"

int		main(int argc, char **argv)
{
	t_scene	scene;

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
			"SDL_Init fail : %s\n", SDL_GetError());
		return (1);
	}
	if (argc < 2)
		no_file();
	read_map(&scene, argv[1]);
	init_scene(&scene, 800, 600);
	init_texture(&scene);
	SDL_SetRenderDrawColor(scene.renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	game_loop(&scene);
	SDL_Quit();
	return (0);
}
