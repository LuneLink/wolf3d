/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_error.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 20:16:14 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 20:16:17 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "error_handler.h"

void	wrong_map_size_param(void)
{
	write(1, "First string in file should"
			"contain h,w param\n", 46);
	exit(0);
}

void	wrong_count_in_line(void)
{
	write(1, "In some line you have not right"
			"count or no \\n at the end \n", 59);
	exit(0);
}

void	wrong_symb_in_line(void)
{
	write(1, "Wrong symbol in line"
			"accepted only 0 and 1\n", 42);
	exit(0);
}

void	no_position_for_player(void)
{
	write(1, "In this map no position for Player"
			"put at lest one 0 not in border\n", 67);
	exit(0);
}

void	map_should_contain_border(void)
{
	write(1, "map should contain border with\n", 31);
	exit(0);
}
