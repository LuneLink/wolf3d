/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_init_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 20:16:44 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 20:16:47 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "error_handler.h"

int	sdl_init_error(void)
{
	return (1);
}

int	sdl_render_create_error(void)
{
	return (1);
}
