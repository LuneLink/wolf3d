/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 20:14:26 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 20:14:28 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "player.h"

void		rotate(t_player *p, int is_right)
{
	float o_d_x;
	float o_p_x;
	float rot_speed;

	o_d_x = p->dir_x;
	o_p_x = p->plane_x;
	rot_speed = is_right ? -R_SPEED : R_SPEED;
	p->dir_x = o_d_x * cos(rot_speed) - p->dir_y * sin(rot_speed);
	p->dir_y = o_d_x * sin(rot_speed) + p->dir_y * cos(rot_speed);
	p->plane_x = o_p_x * cos(rot_speed) - p->plane_y * sin(rot_speed);
	p->plane_y = o_p_x * sin(rot_speed) + p->plane_y * cos(rot_speed);
}

void		move(t_player *p, char **map, int is_toward)
{
	float step_x;
	float step_y;

	step_x = is_toward ? p->dir_x * -0.1f : p->dir_x * 0.1f;
	step_y = is_toward ? p->dir_y * -0.1f : p->dir_y * 0.1f;
	if (map[(int)(p->pos_x + step_x)][(int)(p->pos_y)] == '0')
		p->pos_x += step_x;
	if (map[(int)(p->pos_x)][(int)(p->pos_y + step_y)] == '0')
		p->pos_y += step_y;
}
