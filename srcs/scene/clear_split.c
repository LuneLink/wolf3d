/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clear_split.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 19:30:40 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 19:34:32 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scene.h"

int	clear_split(char ***splited)
{
	char **data;

	data = *splited;
	while (*data)
	{
		free(*data);
		data++;
	}
	free(*splited);
	*splited = 0;
	return (0);
}
