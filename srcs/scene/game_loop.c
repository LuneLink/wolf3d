/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/20 14:24:31 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/20 14:24:33 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scene.h"

static void		calculate_render(t_scene *scene, t_render *r,
		t_player *p, int x)
{
	r->camera_x = 2 * x / (float)scene->w - 1;
	r->ray_d_x = p->dir_x + p->plane_x * r->camera_x;
	r->ray_d_y = p->dir_y + p->plane_y * r->camera_x;
	r->map_x = (int)p->pos_x;
	r->map_y = (int)p->pos_y;
	r->delta_dist_x = ABS(1 / r->ray_d_x);
	r->delta_dist_y = ABS(1 / r->ray_d_y);
	r->hit = 0;
	r->step_x = -1;
	r->step_y = -1;
	if (r->ray_d_x < 0)
		r->side_dist_x = (p->pos_x - r->map_x) * r->delta_dist_x;
	else
	{
		r->step_x = 1;
		r->side_dist_x = (r->map_x + 1.0f - p->pos_x) * r->delta_dist_x;
	}
	if (r->ray_d_y < 0)
		r->side_dist_y = (p->pos_y - r->map_y) * r->delta_dist_y;
	else
	{
		r->step_y = 1;
		r->side_dist_y = (r->map_y + 1.0f - p->pos_y) * r->delta_dist_y;
	}
}

static void		calculate_hit(t_scene *scene, t_render *r, t_player *p)
{
	while (r->hit == 0)
	{
		if (r->side_dist_x < r->side_dist_y)
		{
			r->side_dist_x += r->delta_dist_x;
			r->map_x += r->step_x;
			r->side = 0;
		}
		else
		{
			r->side_dist_y += r->delta_dist_y;
			r->map_y += r->step_y;
			r->side = 1;
		}
		if (scene->map[r->map_x][r->map_y] == '1')
			r->hit = 1;
	}
	if (r->side == 0)
		r->prep_wall_dist = (r->map_x - p->pos_x
							+ (1.f - r->step_x) / 2) / r->ray_d_x;
	else
		r->prep_wall_dist = (r->map_y - p->pos_y
							+ (1.f - r->step_y) / 2) / r->ray_d_y;
}

static void		apply_texture(t_scene *s, t_render *r, int x, int l_hight)
{
	r->wall_x = r->side == 0 ? s->player.pos_y + r->prep_wall_dist * r->ray_d_y
							: s->player.pos_x + r->prep_wall_dist * r->ray_d_x;
	r->wall_x -= floorf((r->wall_x));
	r->tex_x = (int)(r->wall_x * (float)s->text_w);
	if (r->side == 0 && r->ray_d_x > 0)
		r->tex_x = s->text_w - r->tex_x - 1;
	if (r->side == 1 && r->ray_d_y < 0)
		r->tex_x = s->text_w - r->tex_x - 1;
	r->y_ebani = r->draw_s - 1;
	while (++r->y_ebani < r->draw_e)
	{
		r->tex_numb = 3;
		r->d = r->y_ebani * 256 - s->h * 128 + l_hight * 128;
		r->tex_y = ((r->d * s->text_h) / l_hight) / 256;
		if (r->side == 0 && r->ray_d_x >= 0)
			r->tex_numb = 0;
		else if (r->side == 0 && r->ray_d_x < 0)
			r->tex_numb = 1;
		else if (r->side == 1 && r->ray_d_y >= 0)
			r->tex_numb = 2;
		r->color = s->textures[r->tex_numb][s->text_h * r->tex_y + r->tex_x];
		if (r->side == 1)
			r->color = (r->color >> 1) & 8355711;
		putpixels(s->surface, x, r->y_ebani, r->color);
	}
}

void			render_scene(t_scene *scene, int x)
{
	int line_h;

	while (++x < scene->w)
	{
		calculate_render(scene, &(scene->render), &(scene->player), x);
		calculate_hit(scene, &(scene->render), &(scene->player));
		line_h = (int)(scene->h / scene->render.prep_wall_dist);
		scene->render.draw_s = -line_h / 2 + scene->h / 2;
		if (scene->render.draw_s < 0)
			scene->render.draw_s = 0;
		scene->render.draw_e = line_h / 2 + scene->h / 2;
		if (scene->render.draw_e >= scene->h)
			scene->render.draw_e = scene->h - 1;
		apply_texture(scene, &(scene->render), x, line_h);
	}
	SDL_UpdateWindowSurface(scene->window);
}

void			game_loop(t_scene *scene)
{
	static int	done = 0;
	SDL_Event	e;

	while (!done)
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT ||
				((e.type == SDL_KEYDOWN) && (e.key.keysym.sym == SDLK_ESCAPE)))
			{
				done = 1;
				break ;
			}
			if ((e.type == SDL_KEYDOWN) && (e.key.keysym.sym == SDLK_w))
				move(&(scene->player), scene->map, 0);
			if ((e.type == SDL_KEYDOWN) && (e.key.keysym.sym == SDLK_s))
				move(&(scene->player), scene->map, 1);
			if ((e.type == SDL_KEYDOWN) && (e.key.keysym.sym == SDLK_a))
				rotate(&(scene->player), 1);
			if ((e.type == SDL_KEYDOWN) && (e.key.keysym.sym == SDLK_d))
				rotate(&(scene->player), 0);
			SDL_RenderClear(scene->renderer);
			render_scene(scene, -1);
		}
}
