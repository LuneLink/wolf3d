/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_scene.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 19:39:59 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 19:40:01 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scene.h"

static inline int	init_player(t_scene *scene)
{
	int i;
	int j;

	i = -1;
	while (++i < scene->map_h)
	{
		j = -1;
		while (++j < scene->map_w)
		{
			if (scene->map[i][j] == '0')
			{
				scene->player.pos_x = i + 0.5f;
				scene->player.pos_y = j + 0.5f;
				scene->player.dir_x = 1;
				scene->player.dir_y = 0;
				scene->player.plane_x = 0;
				scene->player.plane_y = 0.66;
				return (1);
			}
		}
	}
	return (0);
}

int					init_scene(t_scene *scene, int w, int h)
{
	if (!init_player(scene))
		no_position_for_player();
	scene->w = w;
	scene->h = h;
	scene->text_w = 64;
	scene->text_h = 64;
	scene->window = SDL_CreateWindow("Wolf3D",
								SDL_WINDOWPOS_CENTERED,
								SDL_WINDOWPOS_CENTERED,
								w,
								h,
								0);
	if (!scene->window)
		return (sdl_init_error());
	scene->surface = SDL_GetWindowSurface(scene->window);
	scene->renderer = SDL_CreateSoftwareRenderer(scene->surface);
	return (0);
}
