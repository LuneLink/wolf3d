/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_texture.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 19:40:10 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 19:40:11 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scene.h"

static void		init_last_part(t_scene *scene, int y, int x, int *colors)
{
	scene->textures[3][scene->text_w * y + x] = colors[0] +
			256 * colors[0] + 65536 * colors[0];
	scene->textures[4][scene->text_w * y + x] = 256 * colors[0];
	scene->textures[5][scene->text_w * y + x] = 65536 * 192
			* (x % 16 && y % 16);
	scene->textures[6][scene->text_w * y + x] = 65536 * colors[1];
	scene->textures[7][scene->text_w * y + x] = 128 +
			256 * 128 + 65536 * 128;
}

void			init_texture(t_scene *scene)
{
	int colors[3];
	int x;
	int y;

	x = -1;
	while (++x < scene->text_h)
	{
		y = -1;
		while (++y < scene->text_w)
		{
			colors[0] = (x * 256 / scene->text_w) ^ (y * 256 / scene->text_h);
			colors[1] = y * 256 / scene->text_h;
			colors[2] = y * 128 / scene->text_h + x * 128 / scene->text_w;
			scene->textures[0][scene->text_w * y + x] = 65536 * 254
					* (x != y && x != scene->text_w - y);
			scene->textures[1][scene->text_w * y + x] = colors[2]
					+ 256 * colors[2] + 65536 * colors[2];
			scene->textures[2][scene->text_w * y + x] = 256
					* colors[2] + 65536 * colors[2];
			init_last_part(scene, y, x, colors);
		}
	}
}
