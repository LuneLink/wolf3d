/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_pixel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/20 14:21:11 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/20 14:22:54 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scene.h"

void		putpixels(SDL_Surface *screen, int x, int y, unsigned int color)
{
	unsigned int	*ptr;
	int				line_offset;

	ptr = (unsigned int*)screen->pixels;
	line_offset = y * (screen->pitch / sizeof(unsigned int));
	ptr[line_offset + x] = color;
}
