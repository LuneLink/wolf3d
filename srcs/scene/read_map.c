/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 19:40:19 by spetrenk          #+#    #+#             */
/*   Updated: 2019/04/19 19:40:21 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scene.h"

static inline int	validate_size(char **str, int size)
{
	int i;

	i = 0;
	while (*str)
	{
		i++;
		str++;
	}
	return (i == size);
}

static inline int	initialize_map_w_h(t_scene *scene, char *file)
{
	int		fd;
	char	*line;
	char	**size;

	fd = open(file, O_RDONLY);
	if (errno || fd < 0)
		no_file();
	get_next_line(fd, &line);
	size = ft_strsplit(line, ' ');
	if (!validate_size(size, 2))
		wrong_map_size_param();
	scene->map_h = ft_atoi(size[0]);
	scene->map_w = ft_atoi(size[1]);
	scene->map = (char **)malloc(sizeof(char *) * (scene->map_h + 1));
	scene->map[scene->h] = 0;
	clear_split(&size);
	free(line);
	return (fd);
}

static inline char	*generate_map_row(int fd, int width)
{
	char	*line;
	int		i;

	line = 0;
	get_next_line(fd, &line);
	if (ft_strlen(line) != (size_t)width)
		wrong_count_in_line();
	i = -1;
	while (++i < width)
		if (line[i] != '1' && line[i] != '0')
			wrong_symb_in_line();
	return (line);
}

static inline void	check_map_border(t_scene *scene)
{
	int i;

	i = -1;
	while (++i < scene->map_h)
		if (scene->map[i][0] != '1' || scene->map[i][scene->map_w - 1] != '1')
			map_should_contain_border();
	i = -1;
	while (++i < scene->map_w)
		if (scene->map[0][i] != '1' || scene->map[scene->map_h - 1][i] != '1')
			map_should_contain_border();
}

void				read_map(t_scene *scene, char *file)
{
	int fd;
	int i;

	errno = 0;
	fd = initialize_map_w_h(scene, file);
	if (scene->map_h < 1 && scene->map_w < 1)
		wrong_map_size_param();
	i = -1;
	while (++i < scene->map_h)
	{
		scene->map[i] = generate_map_row(fd, scene->map_w);
	}
	check_map_border(scene);
}
